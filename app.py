import boto3
import hashlib
import json
import os
import requests
import time

from aws_xray_sdk.core import patch, xray_recorder
from aws_xray_sdk.ext.flask.middleware import XRayMiddleware
from botocore.exceptions import ClientError
from flask import Flask, jsonify, request

# Path libraries for x-ray tracking
libs_to_patch = ("boto3", "requests")
patch(libs_to_patch)

app = Flask(__name__)

xray_recorder.configure(service="flask-app")
XRayMiddleware(app, xray_recorder)

USERS_BUCKET = os.environ["USERS_BUCKET"]
USERS_TABLE = os.environ["USERS_TABLE"]
ALERT_TOPIC_ARN = os.environ["ALERT_TOPIC_ARN"]

dynamodb = boto3.client("dynamodb")
s3 = boto3.client("s3")
sns = boto3.client("sns")


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/users/<string:user_id>")
def get_user(user_id):
    response = dynamodb.get_item(
        TableName=USERS_TABLE,
        Key={
            "userId": {"S": user_id}
        }
    )

    item = response.get("Item")

    if not item:
        return jsonify({"error": "User does not exist"}), 404

    response = s3.get_object(
        Bucket=USERS_BUCKET,
        Key=user_id
    )

    body = response.get("Body")

    if not body:
        return jsonify({"error": "User does not exist"}), 404

    user = json.loads(body.read())

    gravatar_id = get_gravatar(user_id).get("id", "NA")

    return jsonify({
        "userId": item.get("userId").get("S"),
        "name": item.get("name").get("S"),
        "created": user.get("created"),
        "gravatarId": gravatar_id
    })


@app.route("/users", methods=["POST"])
def create_user():
    print(request.json)
    user_id = request.json.get("userId")
    name = request.json.get("name")

    if not user_id or not name:
        return jsonify({"error": "userId and name are required!"}), 400

    try:
        dynamodb.put_item(
            TableName=USERS_TABLE,
            Item={
                "userId": {"S": user_id},
                "name": {"S": name},
            }
        )

        content = {
            "user_id": user_id,
            "name": name,
            "created": int(time.time())
        }

        s3.put_object(
            Bucket=USERS_BUCKET,
            Key=user_id,
            Body=json.dumps(content, indent=4)
        )

    except ClientError as e:
        print(e)
        return jsonify({"error": "Unable to create user!"}), 500


    sns.publish(
        TopicArn=ALERT_TOPIC_ARN,
        Message="New user {} created.".format(user_id),
        Subject="Serverless X-Ray Tester New User Alert"
    )

    return jsonify({
        "userId": user_id,
        "name": name
    })


@xray_recorder.capture("get gravatar profile")
def get_gravatar(user_id):
    hash = hashlib.md5("{}@test.com".format(user_id).encode()).hexdigest()
    url = "https://www.gravatar.com/{}.json".format(hash)
    res = requests.get(url)

    if res.status_code == 200:
        return res.json().get("entry", [])[0]

    return {}
