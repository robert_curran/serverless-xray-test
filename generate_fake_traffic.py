import requests
import sys
from faker import Faker

BASE_URL = "TODO REPLACE"

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: {} <POST count> <GET count>".format(sys.argv[0]))
        sys.exit(1)

    post_count = int(sys.argv[1])
    get_count = int(sys.argv[2])

    if get_count > post_count:
        print("Get count cannot be greater than post count!")
        sys.exit(1)

    names = []

    fake = Faker()

    for x in range(post_count):
        name = "{} {}".format(fake.first_name(), fake.last_name())
        id = name.lower().replace(" ", "")
        names.append(name)

    print("Generating users...")

    for name in names:
        user_id = name.lower().replace(" ", "")

        url = "{}/users".format(BASE_URL)

        data = {
            "name": name,
            "userId": user_id
        }

        requests.post(url, json=data)

    print("Getting users...")
    for x in range(get_count):
        name = names[x]
        user_id = name.lower().replace(" ", "")

        url = "{}/users/{}".format(BASE_URL, user_id)

        requests.get(url)

    print("Done")
