# Serverless X-Ray Test

Example serverless Flask AWS Lambda application to test/demo X-Ray distributed application tracing.

The application has two endpoints, one to get a users details and one to create a users.

```bash
GET  /users/<user_id>
POST /users/
```

## Create User

Users are created by posting JSON to the `/users` endpoint. The users details are stored in both DynamoDB and S3. Additionally a message is published to an SNS queue when a new user is created.
The JSON body for the POST request looks like:

```json
{
    "name": "Joe Bloggs",
    "userId": "joebloggs"
}
```

## Get User

User details are retrieved by making a request to the `/users/<user_id>` endpoint. The returned user contains details from both DynamoDB, S3 and by making an API request to Gravatar to show how tracing works with external APIs as well as other AWS services.
The GET response looks like:

```json
{
    "created": 1561462390,
    "gravatarId": "NA",
    "name": "Joe Bloggs",
    "userId":"joebloggs"
}
```

## Deploying the application

The application is deployed using the serverless framework by running `sls deploy` from the project root directory.

Before deploying make sure to update the custom variables for `SNS Topic`, `Alerts Email`, `DynamoDB Table` and `S3 Bucket` in `serverless.yml` to something unique for your account.

After deploying take note of the `endpoints:` in the output the first one (ending in `/dev`) is the base url for all requests.

## Generating traffic

In order to see any kind of useful X-Ray trace we fist need some traffic to hit the application endpoints to generate data for the trace. There is a script (`generate_fake_traffic.py`) is included. This will generate a specified number of POST and GET requests using Faker to generate fake user details.

Before running the script you need to update the `BASE_URL` variable in the file with the one output from the deployment.

The script is run like so:

```bash
python generate_fake_traffic.py <post count> <get count>
```

## Example traces

Once traffic has been generated you can view the traces in the X-Ray console. Below are examples of the service map and details of an individual requests trace that were captured in this way.

### Service map

![service map](xray-service-map.png)

Note: each request to Gravatar is a unique url and therefore shows as a unique service in the service map.

### Request trace details

![request trace details](xray-trace-details.png)

Note: here we see a detailed timeline of each part of the request, DynamoDB GetItem, S3 GetObject and Gravatar profile request as well as the APIGateway and Lambda function invocations.
